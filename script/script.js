class Employee {
    constructor(name, age, salary){
        this._name   = name,
        this._age    = age,
        this._salary = salary
    }  
    get name(){
        return this._name;
    }
    get age(){
        return this._age;
    }
    get salary(){
        return this._salary;
    }
    set name(value) { 
        this._name = value;
    }  
    set age(value){
        this._age = value;
    }
    set salary(value){
        this._salary = value;
    }
}
let worker1 = new Employee('Eduard', 21, 45.12)

console.log(worker1.name) 
console.log(worker1.age)
console.log(worker1.salary)

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }
    get salary(){
        return this._salary*3
    }
}
let programmer1 = new Programmer("Rey", 49, 13.4,"JavaScript")
let programmer2 = new Programmer("Roy", 32, 7,"JavaScript")
console.log(programmer1)
console.log(programmer1.salary)
console.log(programmer2)
console.log(programmer2.salary)
